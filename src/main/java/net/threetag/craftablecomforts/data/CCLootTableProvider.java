package net.threetag.craftablecomforts.data;

import net.minecraft.advancements.criterion.StatePropertiesPredicate;
import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.loot.*;
import net.minecraft.loot.conditions.BlockStateProperty;
import net.minecraft.loot.functions.SetCount;
import net.threetag.craftablecomforts.block.CCBlocks;
import net.threetag.craftablecomforts.block.CandleBlock;

public class CCLootTableProvider extends BaseLootTableProvider {

    public CCLootTableProvider(DataGenerator dataGeneratorIn) {
        super(dataGeneratorIn);
    }

    @Override
    protected void addTables() {
        standardBlockTable(CCBlocks.BENCH.get());
        lootTables.put(CCBlocks.OAK_CRATE.get(), droppingWithName(CCBlocks.OAK_CRATE.get()));
        lootTables.put(CCBlocks.BIRCH_CRATE.get(), droppingWithName(CCBlocks.BIRCH_CRATE.get()));
        lootTables.put(CCBlocks.SPRUCE_CRATE.get(), droppingWithName(CCBlocks.SPRUCE_CRATE.get()));
        lootTables.put(CCBlocks.JUNGLE_CRATE.get(), droppingWithName(CCBlocks.JUNGLE_CRATE.get()));
        lootTables.put(CCBlocks.ACACIA_CRATE.get(), droppingWithName(CCBlocks.ACACIA_CRATE.get()));
        lootTables.put(CCBlocks.DARK_OAK_CRATE.get(), droppingWithName(CCBlocks.DARK_OAK_CRATE.get()));
        lootTables.put(CCBlocks.CRIMSON_CRATE.get(), droppingWithName(CCBlocks.CRIMSON_CRATE.get()));
        lootTables.put(CCBlocks.WARPED_CRATE.get(), droppingWithName(CCBlocks.WARPED_CRATE.get()));
        standardBlockTable(CCBlocks.BLOCK_RAILING.get());
        standardBlockTable(CCBlocks.ORNATE_RAILING.get());
        standardBlockTable(CCBlocks.WAVE_RAILING.get());
        standardBlockTable(CCBlocks.CHAINED_RAILING.get());
        lootTables.put(CCBlocks.CANDLE.get(), candleDrops(CCBlocks.CANDLE.get()));
        lootTables.put(CCBlocks.WHITE_CANDLE.get(), candleDrops(CCBlocks.WHITE_CANDLE.get()));
        lootTables.put(CCBlocks.ORANGE_CANDLE.get(), candleDrops(CCBlocks.ORANGE_CANDLE.get()));
        lootTables.put(CCBlocks.MAGENTA_CANDLE.get(), candleDrops(CCBlocks.MAGENTA_CANDLE.get()));
        lootTables.put(CCBlocks.LIGHT_BLUE_CANDLE.get(), candleDrops(CCBlocks.LIGHT_BLUE_CANDLE.get()));
        lootTables.put(CCBlocks.YELLOW_CANDLE.get(), candleDrops(CCBlocks.YELLOW_CANDLE.get()));
        lootTables.put(CCBlocks.LIME_CANDLE.get(), candleDrops(CCBlocks.LIME_CANDLE.get()));
        lootTables.put(CCBlocks.PINK_CANDLE.get(), candleDrops(CCBlocks.PINK_CANDLE.get()));
        lootTables.put(CCBlocks.GRAY_CANDLE.get(), candleDrops(CCBlocks.GRAY_CANDLE.get()));
        lootTables.put(CCBlocks.LIGHT_GRAY_CANDLE.get(), candleDrops(CCBlocks.LIGHT_GRAY_CANDLE.get()));
        lootTables.put(CCBlocks.CYAN_CANDLE.get(), candleDrops(CCBlocks.CYAN_CANDLE.get()));
        lootTables.put(CCBlocks.PURPLE_CANDLE.get(), candleDrops(CCBlocks.PURPLE_CANDLE.get()));
        lootTables.put(CCBlocks.BLUE_CANDLE.get(), candleDrops(CCBlocks.BLUE_CANDLE.get()));
        lootTables.put(CCBlocks.BROWN_CANDLE.get(), candleDrops(CCBlocks.BROWN_CANDLE.get()));
        lootTables.put(CCBlocks.GREEN_CANDLE.get(), candleDrops(CCBlocks.GREEN_CANDLE.get()));
        lootTables.put(CCBlocks.RED_CANDLE.get(), candleDrops(CCBlocks.RED_CANDLE.get()));
        lootTables.put(CCBlocks.BLACK_CANDLE.get(), candleDrops(CCBlocks.BLACK_CANDLE.get()));

        lootTables.put(CCBlocks.CANDLE_CAKE.get(), candleCakeDrops(CCBlocks.CANDLE_CAKE.get()));
        lootTables.put(CCBlocks.WHITE_CANDLE_CAKE.get(), candleCakeDrops(CCBlocks.WHITE_CANDLE_CAKE.get()));
        lootTables.put(CCBlocks.ORANGE_CANDLE_CAKE.get(), candleCakeDrops(CCBlocks.ORANGE_CANDLE_CAKE.get()));
        lootTables.put(CCBlocks.MAGENTA_CANDLE_CAKE.get(), candleCakeDrops(CCBlocks.MAGENTA_CANDLE_CAKE.get()));
        lootTables.put(CCBlocks.LIGHT_BLUE_CANDLE_CAKE.get(), candleCakeDrops(CCBlocks.LIGHT_BLUE_CANDLE_CAKE.get()));
        lootTables.put(CCBlocks.YELLOW_CANDLE_CAKE.get(), candleCakeDrops(CCBlocks.YELLOW_CANDLE_CAKE.get()));
        lootTables.put(CCBlocks.LIME_CANDLE_CAKE.get(), candleCakeDrops(CCBlocks.LIME_CANDLE_CAKE.get()));
        lootTables.put(CCBlocks.PINK_CANDLE_CAKE.get(), candleCakeDrops(CCBlocks.PINK_CANDLE_CAKE.get()));
        lootTables.put(CCBlocks.GRAY_CANDLE_CAKE.get(), candleCakeDrops(CCBlocks.GRAY_CANDLE_CAKE.get()));
        lootTables.put(CCBlocks.LIGHT_GRAY_CANDLE_CAKE.get(), candleCakeDrops(CCBlocks.LIGHT_GRAY_CANDLE_CAKE.get()));
        lootTables.put(CCBlocks.CYAN_CANDLE_CAKE.get(), candleCakeDrops(CCBlocks.CYAN_CANDLE_CAKE.get()));
        lootTables.put(CCBlocks.PURPLE_CANDLE_CAKE.get(), candleCakeDrops(CCBlocks.PURPLE_CANDLE_CAKE.get()));
        lootTables.put(CCBlocks.BLUE_CANDLE_CAKE.get(), candleCakeDrops(CCBlocks.BLUE_CANDLE_CAKE.get()));
        lootTables.put(CCBlocks.BROWN_CANDLE_CAKE.get(), candleCakeDrops(CCBlocks.BROWN_CANDLE_CAKE.get()));
        lootTables.put(CCBlocks.GREEN_CANDLE_CAKE.get(), candleCakeDrops(CCBlocks.GREEN_CANDLE_CAKE.get()));
        lootTables.put(CCBlocks.RED_CANDLE_CAKE.get(), candleCakeDrops(CCBlocks.RED_CANDLE_CAKE.get()));
        lootTables.put(CCBlocks.BLACK_CANDLE_CAKE.get(), candleCakeDrops(CCBlocks.BLACK_CANDLE_CAKE.get()));
    }

    private static LootTable.Builder candleDrops(Block candle) {
        return LootTable.builder()
                .addLootPool(LootPool.builder()
                        .rolls(ConstantRange.of(1))
                        .addEntry(withExplosionDecay(candle, ItemLootEntry.builder(candle)
                                .acceptFunction(SetCount.builder(ConstantRange.of(2))
                                        .acceptCondition(BlockStateProperty.builder(candle)
                                                .fromProperties(StatePropertiesPredicate.Builder.newBuilder()
                                                        .withIntProp(CandleBlock.CANDLES, 2))))
                                .acceptFunction(SetCount.builder(ConstantRange.of(3))
                                        .acceptCondition(BlockStateProperty.builder(candle)
                                                .fromProperties(StatePropertiesPredicate.Builder.newBuilder()
                                                        .withIntProp(CandleBlock.CANDLES, 3))))
                                .acceptFunction(SetCount.builder(ConstantRange.of(4))
                                        .acceptCondition(BlockStateProperty.builder(candle)
                                                .fromProperties(StatePropertiesPredicate.Builder.newBuilder()
                                                        .withIntProp(CandleBlock.CANDLES, 4)))))));
    }

    private static LootTable.Builder candleCakeDrops(Block candle) {
        return LootTable.builder().addLootPool(LootPool.builder().rolls(ConstantRange.of(1)).addEntry(ItemLootEntry.builder(candle)));
    }

    @Override
    public String getName() {
        return "CraftableComforts Loot Tables";
    }
}
