package net.threetag.craftablecomforts.data;

import net.minecraft.data.BlockTagsProvider;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.threetag.craftablecomforts.CraftableComforts;
import net.threetag.craftablecomforts.block.CCBlocks;
import net.threetag.craftablecomforts.tags.CCBlockTags;

import javax.annotation.Nullable;

public class CCBlockTagProvider extends BlockTagsProvider {

    public CCBlockTagProvider(DataGenerator generatorIn, @Nullable ExistingFileHelper existingFileHelper) {
        super(generatorIn, CraftableComforts.MODID, existingFileHelper);
    }

    @Override
    protected void registerTags() {
        this.getOrCreateBuilder(CCBlockTags.CANDLES).add(CCBlocks.CANDLE.get(), CCBlocks.WHITE_CANDLE.get(), CCBlocks.ORANGE_CANDLE.get(), CCBlocks.MAGENTA_CANDLE.get(), CCBlocks.LIGHT_BLUE_CANDLE.get(), CCBlocks.YELLOW_CANDLE.get(), CCBlocks.LIME_CANDLE.get(), CCBlocks.PINK_CANDLE.get(), CCBlocks.GRAY_CANDLE.get(), CCBlocks.LIGHT_GRAY_CANDLE.get(), CCBlocks.CYAN_CANDLE.get(), CCBlocks.PURPLE_CANDLE.get(), CCBlocks.BLUE_CANDLE.get(), CCBlocks.BROWN_CANDLE.get(), CCBlocks.GREEN_CANDLE.get(), CCBlocks.RED_CANDLE.get(), CCBlocks.BLACK_CANDLE.get());
        this.getOrCreateBuilder(CCBlockTags.CANDLE_CAKES).add(CCBlocks.CANDLE_CAKE.get(), CCBlocks.WHITE_CANDLE_CAKE.get(), CCBlocks.ORANGE_CANDLE_CAKE.get(), CCBlocks.MAGENTA_CANDLE_CAKE.get(), CCBlocks.LIGHT_BLUE_CANDLE_CAKE.get(), CCBlocks.YELLOW_CANDLE_CAKE.get(), CCBlocks.LIME_CANDLE_CAKE.get(), CCBlocks.PINK_CANDLE_CAKE.get(), CCBlocks.GRAY_CANDLE_CAKE.get(), CCBlocks.LIGHT_GRAY_CANDLE_CAKE.get(), CCBlocks.CYAN_CANDLE_CAKE.get(), CCBlocks.PURPLE_CANDLE_CAKE.get(), CCBlocks.BLUE_CANDLE_CAKE.get(), CCBlocks.BROWN_CANDLE_CAKE.get(), CCBlocks.GREEN_CANDLE_CAKE.get(), CCBlocks.RED_CANDLE_CAKE.get(), CCBlocks.BLACK_CANDLE_CAKE.get());
    }

    @Override
    public String getName() {
        return "CraftableComforts " + super.getName();
    }
}
