package net.threetag.craftablecomforts.data;

import net.minecraft.data.*;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.util.IItemProvider;
import net.minecraftforge.common.Tags;
import net.threetag.craftablecomforts.block.CCBlocks;

import java.util.function.Consumer;

public class CCRecipeProvider extends RecipeProvider {

    public CCRecipeProvider(DataGenerator generatorIn) {
        super(generatorIn);
    }

    @Override
    protected void registerRecipes(Consumer<IFinishedRecipe> consumer) {
        ShapedRecipeBuilder.shapedRecipe(CCBlocks.CANDLE.get()).key('S', Items.STRING).key('H', Items.HONEYCOMB).patternLine("S").patternLine("H").addCriterion("has_string", hasItem(Items.STRING)).addCriterion("has_honeycomb", hasItem(Items.HONEYCOMB)).build(consumer);

        candleDyeingRecipe(consumer, Tags.Items.DYES_BLACK, CCBlocks.BLACK_CANDLE.get());
        candleDyeingRecipe(consumer, Tags.Items.DYES_BLUE, CCBlocks.BLUE_CANDLE.get());
        candleDyeingRecipe(consumer, Tags.Items.DYES_BROWN, CCBlocks.BROWN_CANDLE.get());
        candleDyeingRecipe(consumer, Tags.Items.DYES_CYAN, CCBlocks.CYAN_CANDLE.get());
        candleDyeingRecipe(consumer, Tags.Items.DYES_GRAY, CCBlocks.GRAY_CANDLE.get());
        candleDyeingRecipe(consumer, Tags.Items.DYES_GREEN, CCBlocks.GREEN_CANDLE.get());
        candleDyeingRecipe(consumer, Tags.Items.DYES_LIGHT_BLUE, CCBlocks.LIGHT_BLUE_CANDLE.get());
        candleDyeingRecipe(consumer, Tags.Items.DYES_LIGHT_GRAY, CCBlocks.LIGHT_GRAY_CANDLE.get());
        candleDyeingRecipe(consumer, Tags.Items.DYES_LIME, CCBlocks.LIME_CANDLE.get());
        candleDyeingRecipe(consumer, Tags.Items.DYES_MAGENTA, CCBlocks.MAGENTA_CANDLE.get());
        candleDyeingRecipe(consumer, Tags.Items.DYES_ORANGE, CCBlocks.ORANGE_CANDLE.get());
        candleDyeingRecipe(consumer, Tags.Items.DYES_PINK, CCBlocks.PINK_CANDLE.get());
        candleDyeingRecipe(consumer, Tags.Items.DYES_PURPLE, CCBlocks.PURPLE_CANDLE.get());
        candleDyeingRecipe(consumer, Tags.Items.DYES_RED, CCBlocks.RED_CANDLE.get());
        candleDyeingRecipe(consumer, Tags.Items.DYES_WHITE, CCBlocks.WHITE_CANDLE.get());
        candleDyeingRecipe(consumer, Tags.Items.DYES_YELLOW, CCBlocks.YELLOW_CANDLE.get());
    }

    public static void candleDyeingRecipe(Consumer<IFinishedRecipe> exporter, Tags.IOptionalNamedTag<Item> color, IItemProvider outputItem) {
        ShapelessRecipeBuilder.shapelessRecipe(outputItem).addIngredient(CCBlocks.CANDLE.get()).addIngredient(color).addCriterion("has_" + color.getName().getPath(), hasItem(color)).build(exporter);
    }

    @Override
    public String getName() {
        return "CraftableComforts " + super.getName();
    }
}
