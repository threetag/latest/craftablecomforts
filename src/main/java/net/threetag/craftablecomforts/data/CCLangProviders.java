package net.threetag.craftablecomforts.data;

import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.data.LanguageProvider;
import net.threetag.craftablecomforts.CraftableComforts;
import net.threetag.craftablecomforts.block.CCBlocks;

public abstract class CCLangProviders extends LanguageProvider {

    public CCLangProviders(DataGenerator gen, String locale) {
        super(gen, CraftableComforts.MODID, locale);
    }

    public static class English extends CCLangProviders {

        public English(DataGenerator gen) {
            super(gen, "en_us");
        }

        @Override
        protected void addTranslations() {
            this.addBlock(CCBlocks.BENCH, "Bench");
            this.addBlock(CCBlocks.OAK_CRATE, "Oak Crate");
            this.addBlock(CCBlocks.SPRUCE_CRATE, "Spruce Crate");
            this.addBlock(CCBlocks.BIRCH_CRATE, "Birch Crate");
            this.addBlock(CCBlocks.JUNGLE_CRATE, "Jungle Crate");
            this.addBlock(CCBlocks.ACACIA_CRATE, "Acacia Crate");
            this.addBlock(CCBlocks.DARK_OAK_CRATE, "Dark Oak Crate");
            this.addBlock(CCBlocks.CRIMSON_CRATE, "Crimson Crate");
            this.addBlock(CCBlocks.WARPED_CRATE, "Warped Crate");
            this.addBlock(CCBlocks.BLOCK_RAILING, "Block Railing");
            this.addBlock(CCBlocks.ORNATE_RAILING, "Ornate Railing");
            this.addBlock(CCBlocks.WAVE_RAILING, "Wave Railing");
            this.addBlock(CCBlocks.CHAINED_RAILING, "Chained Railing");

            this.addBlock(CCBlocks.CANDLE, "Candle");
            this.addBlock(CCBlocks.WHITE_CANDLE, "White Candle");
            this.addBlock(CCBlocks.ORANGE_CANDLE, "Orange Candle");
            this.addBlock(CCBlocks.MAGENTA_CANDLE, "Magenta Candle");
            this.addBlock(CCBlocks.LIGHT_BLUE_CANDLE, "Light Blue Candle");
            this.addBlock(CCBlocks.YELLOW_CANDLE, "Yellow Candle");
            this.addBlock(CCBlocks.LIME_CANDLE, "Lime Candle");
            this.addBlock(CCBlocks.PINK_CANDLE, "Pink Candle");
            this.addBlock(CCBlocks.GRAY_CANDLE, "Gray Candle");
            this.addBlock(CCBlocks.LIGHT_GRAY_CANDLE, "Light Gray Candle");
            this.addBlock(CCBlocks.CYAN_CANDLE, "Cyan Candle");
            this.addBlock(CCBlocks.PURPLE_CANDLE, "Purple Candle");
            this.addBlock(CCBlocks.BLUE_CANDLE, "Blue Candle");
            this.addBlock(CCBlocks.BROWN_CANDLE, "Brown Candle");
            this.addBlock(CCBlocks.GREEN_CANDLE, "Green Candle");
            this.addBlock(CCBlocks.RED_CANDLE, "Red Candle");
            this.addBlock(CCBlocks.BLACK_CANDLE, "Black Candle");

            this.addBlock(CCBlocks.CANDLE_CAKE, "Candle Cake");
            this.addBlock(CCBlocks.WHITE_CANDLE_CAKE, "White Candle Cake");
            this.addBlock(CCBlocks.ORANGE_CANDLE_CAKE, "Orange Candle Cake");
            this.addBlock(CCBlocks.MAGENTA_CANDLE_CAKE, "Magenta Candle Cake");
            this.addBlock(CCBlocks.LIGHT_BLUE_CANDLE_CAKE, "Light Blue Candle Cake");
            this.addBlock(CCBlocks.YELLOW_CANDLE_CAKE, "Yellow Candle Cake");
            this.addBlock(CCBlocks.LIME_CANDLE_CAKE, "Lime Candle Cake");
            this.addBlock(CCBlocks.PINK_CANDLE_CAKE, "Pink Candle Cake");
            this.addBlock(CCBlocks.GRAY_CANDLE_CAKE, "Gray Candle Cake");
            this.addBlock(CCBlocks.LIGHT_GRAY_CANDLE_CAKE, "Light Gray Candle Cake");
            this.addBlock(CCBlocks.CYAN_CANDLE_CAKE, "Cyan Candle Cake");
            this.addBlock(CCBlocks.PURPLE_CANDLE_CAKE, "Purple Candle Cake");
            this.addBlock(CCBlocks.BLUE_CANDLE_CAKE, "Blue Candle Cake");
            this.addBlock(CCBlocks.BROWN_CANDLE_CAKE, "Brown Candle Cake");
            this.addBlock(CCBlocks.GREEN_CANDLE_CAKE, "Green Candle Cake");
            this.addBlock(CCBlocks.RED_CANDLE_CAKE, "Red Candle Cake");
            this.addBlock(CCBlocks.BLACK_CANDLE_CAKE, "Black Candle Cake");

            this.add("subtitles.block.candle.crackle", "Candle crackles");
            this.add("subtitles.block.cake.add_candle", "Cake squishes");

            this.add("container.craftablecomforts.crate", "Crate");
        }
    }

    public static class German extends CCLangProviders {

        public German(DataGenerator gen) {
            super(gen, "de_de");
        }

        @Override
        protected void addTranslations() {
            this.addBlock(CCBlocks.BENCH, "Bank");
            this.addBlock(CCBlocks.OAK_CRATE, "Eichenholzkasten");
            this.addBlock(CCBlocks.SPRUCE_CRATE, "Fichtenholzkasten");
            this.addBlock(CCBlocks.BIRCH_CRATE, "Birkenholzkasten");
            this.addBlock(CCBlocks.JUNGLE_CRATE, "Tropenholzkasten");
            this.addBlock(CCBlocks.ACACIA_CRATE, "Akazienholzkasten");
            this.addBlock(CCBlocks.DARK_OAK_CRATE, "Schwarzeichenholzkasten");
            this.addBlock(CCBlocks.CRIMSON_CRATE, "Karmesinkasten");
            this.addBlock(CCBlocks.WARPED_CRATE, "Wirrkasten");
            this.addBlock(CCBlocks.BLOCK_RAILING, "Eckiges Gel\u00e4nder");
            this.addBlock(CCBlocks.ORNATE_RAILING, "Prunkvolles Gel\u00e4nder");
            this.addBlock(CCBlocks.WAVE_RAILING, "Gewelltes Gel\u00e4nder");
            this.addBlock(CCBlocks.CHAINED_RAILING, "Kettengel\u00e4nder");

            this.addBlock(CCBlocks.CANDLE, "Kerze");
            this.addBlock(CCBlocks.WHITE_CANDLE, "Wei\u00dfe Kerze");
            this.addBlock(CCBlocks.ORANGE_CANDLE, "Orange Kerze");
            this.addBlock(CCBlocks.MAGENTA_CANDLE, "Magenta Kerze");
            this.addBlock(CCBlocks.LIGHT_BLUE_CANDLE, "Hellblaue Kerze");
            this.addBlock(CCBlocks.YELLOW_CANDLE, "Gelbe Kerze");
            this.addBlock(CCBlocks.LIME_CANDLE, "Hellgr\u00fcne Kerze");
            this.addBlock(CCBlocks.PINK_CANDLE, "Rosa Kerze");
            this.addBlock(CCBlocks.GRAY_CANDLE, "Graue Kerze");
            this.addBlock(CCBlocks.LIGHT_GRAY_CANDLE, "Hellgraue Kerze");
            this.addBlock(CCBlocks.CYAN_CANDLE, "T\u00fcrkise Kerze");
            this.addBlock(CCBlocks.PURPLE_CANDLE, "Violette Kerze");
            this.addBlock(CCBlocks.BLUE_CANDLE, "Blaue Kerze");
            this.addBlock(CCBlocks.BROWN_CANDLE, "Braune Kerze");
            this.addBlock(CCBlocks.GREEN_CANDLE, "Gr\u00fcne Kerze");
            this.addBlock(CCBlocks.RED_CANDLE, "Rote Kerze");
            this.addBlock(CCBlocks.BLACK_CANDLE, "Schwarze Kerze");

            this.addBlock(CCBlocks.CANDLE_CAKE, "Kuchen mit Kerze");
            this.addBlock(CCBlocks.WHITE_CANDLE_CAKE, "Kuchen mit wei\u00dfe Kerze");
            this.addBlock(CCBlocks.ORANGE_CANDLE_CAKE, "Kuchen mit oranger Kerze");
            this.addBlock(CCBlocks.MAGENTA_CANDLE_CAKE, "Kuchen mit magenta Kerze");
            this.addBlock(CCBlocks.LIGHT_BLUE_CANDLE_CAKE, "Kuchen mit hellblauer Kerze");
            this.addBlock(CCBlocks.YELLOW_CANDLE_CAKE, "Kuchen mit gelber Kerze");
            this.addBlock(CCBlocks.LIME_CANDLE_CAKE, "Kuchen mit hellgr\u00fcner Kerze");
            this.addBlock(CCBlocks.PINK_CANDLE_CAKE, "Kuchen mit rosa Kerze");
            this.addBlock(CCBlocks.GRAY_CANDLE_CAKE, "Kuchen mit grauer Kerze");
            this.addBlock(CCBlocks.LIGHT_GRAY_CANDLE_CAKE, "Kuchen mit hellgrauer Kerze");
            this.addBlock(CCBlocks.CYAN_CANDLE_CAKE, "Kuchen mit t\u00fcrkiser Kerze");
            this.addBlock(CCBlocks.PURPLE_CANDLE_CAKE, "Kuchen mit violetter Kerze");
            this.addBlock(CCBlocks.BLUE_CANDLE_CAKE, "Kuchen mit blauer Kerze");
            this.addBlock(CCBlocks.BROWN_CANDLE_CAKE, "Kuchen mit brauner Kerze");
            this.addBlock(CCBlocks.GREEN_CANDLE_CAKE, "Kuchen mit gr\u00fcner Kerze");
            this.addBlock(CCBlocks.RED_CANDLE_CAKE, "Kuchen mit roter Kerze");
            this.addBlock(CCBlocks.BLACK_CANDLE_CAKE, "Kuchen mit schwarzer Kerze");

            this.add("subtitles.block.candle.crackle", "Kerze knistert");
            this.add("subtitles.block.cake.add_candle", "Kuchen schwabbelt");

            this.add("container.craftablecomforts.crate", "Kasten");
        }
    }

    @Override
    public String getName() {
        return "CraftableComforts " + super.getName();
    }
}
