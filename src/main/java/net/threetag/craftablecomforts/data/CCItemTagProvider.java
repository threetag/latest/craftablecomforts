package net.threetag.craftablecomforts.data;

import net.minecraft.data.BlockTagsProvider;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.ItemTagsProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.threetag.craftablecomforts.CraftableComforts;
import net.threetag.craftablecomforts.tags.CCBlockTags;
import net.threetag.craftablecomforts.tags.CCItemTags;

import javax.annotation.Nullable;

public class CCItemTagProvider extends ItemTagsProvider {

    public CCItemTagProvider(DataGenerator dataGenerator, BlockTagsProvider blockTagProvider, @Nullable ExistingFileHelper existingFileHelper) {
        super(dataGenerator, blockTagProvider, CraftableComforts.MODID, existingFileHelper);
    }

    @Override
    protected void registerTags() {
        this.copy(CCBlockTags.CANDLES, CCItemTags.CANDLES);
    }

    @Override
    public String getName() {
        return "CraftableComforts " + super.getName();
    }

}
