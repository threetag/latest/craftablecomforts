package net.threetag.craftablecomforts.item;

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public class SortedBlockItem extends BlockItem {

    public final ItemGroupFiller filler;

    public SortedBlockItem(Block blockIn, ItemGroupFiller filler, Properties builder) {
        super(blockIn, builder);
       this.filler = filler;
    }

    @Override
    public void fillItemGroup(ItemGroup group, NonNullList<ItemStack> items) {
        if (this.isInGroup(group)) {
            this.filler.fill(items, new ItemStack(this));
        }
    }
}
