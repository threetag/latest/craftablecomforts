package net.threetag.craftablecomforts.sound;

import net.minecraft.block.SoundType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.common.util.ForgeSoundType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.threetag.craftablecomforts.CraftableComforts;

public class CCSoundEvents {

    public static final DeferredRegister<SoundEvent> SOUND_EVENTS = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, CraftableComforts.MODID);

    public static final RegistryObject<SoundEvent> BLOCK_CANDLE_AMBIENT = register("block.candle.ambient");
    public static final RegistryObject<SoundEvent> BLOCK_CANDLE_BREAK = register("block.candle.break");
    public static final RegistryObject<SoundEvent> BLOCK_CANDLE_EXTINGUISH = register("block.candle.extinguish");
    public static final RegistryObject<SoundEvent> BLOCK_CANDLE_FALL = register("block.candle.fall");
    public static final RegistryObject<SoundEvent> BLOCK_CANDLE_HIT = register("block.candle.hit");
    public static final RegistryObject<SoundEvent> BLOCK_CANDLE_PLACE = register("block.candle.place");
    public static final RegistryObject<SoundEvent> BLOCK_CANDLE_STEP = register("block.candle.step");
    public static final RegistryObject<SoundEvent> BLOCK_CAKE_ADD_CANDLE = register("block.cake.add_candle");

    public static final SoundType CANDLE = new ForgeSoundType(1.0F, 1.0F, CCSoundEvents.BLOCK_CANDLE_BREAK, CCSoundEvents.BLOCK_CANDLE_STEP, CCSoundEvents.BLOCK_CANDLE_PLACE, CCSoundEvents.BLOCK_CANDLE_HIT, CCSoundEvents.BLOCK_CANDLE_FALL);

    public static RegistryObject<SoundEvent> register(String name) {
        return SOUND_EVENTS.register(name, () -> new SoundEvent(new ResourceLocation(CraftableComforts.MODID, name)));
    }

}
