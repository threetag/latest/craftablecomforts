package net.threetag.craftablecomforts.entity;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.threetag.craftablecomforts.block.ISitableBlock;

public class SitableEntity extends Entity {

    public SitableEntity(EntityType<?> entityTypeIn, World worldIn) {
        super(entityTypeIn, worldIn);
    }

    public SitableEntity(World world, BlockPos pos, LivingEntity entity) {
        super(CCEntityTypes.SITABLE.get(), world);
        this.setPosition(pos.getX() + 0.5F, pos.getY(), pos.getZ() + 0.5F);
        entity.startRiding(this);
    }

    @Override
    public void tick() {
        super.tick();

        if (!this.world.isRemote && this.isAlive()) {
            if(this.getPassengers().size() != 1) {
                this.setDead();
            } else {
                BlockPos pos = this.getPosition();
                BlockState state = this.world.getBlockState(pos);

                if (state.getBlock() instanceof ISitableBlock) {
                    this.setPosition(pos.getX() + 0.5F, pos.getY() + ((ISitableBlock) state.getBlock()).getSitOffset(state, pos), pos.getZ() + 0.5F);
                } else {
                    this.setDead();
                }
            }
        }
    }

    @Override
    protected void registerData() {

    }

    @Override
    protected void readAdditional(CompoundNBT compound) {

    }

    @Override
    protected void writeAdditional(CompoundNBT compound) {

    }

    @Override
    public IPacket<?> createSpawnPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }
}
