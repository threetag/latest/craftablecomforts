package net.threetag.craftablecomforts.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.threetag.craftablecomforts.CraftableComforts;
import net.threetag.craftablecomforts.client.renderer.entity.InvisibleRenderer;

import java.util.function.Supplier;

public class CCEntityTypes {

    public static final DeferredRegister<EntityType<?>> ENTITY_TYPES = DeferredRegister.create(ForgeRegistries.ENTITIES, CraftableComforts.MODID);

    public static final RegistryObject<EntityType<SitableEntity>> SITABLE = register("sitable", () -> EntityType.Builder.<SitableEntity>create(SitableEntity::new, EntityClassification.MISC).size(0.2F, 0.2F));

    @OnlyIn(Dist.CLIENT)
    public static void initRenderers() {
        RenderingRegistry.registerEntityRenderingHandler(SITABLE.get(), InvisibleRenderer::new);
    }

    public static <T extends Entity> RegistryObject<EntityType<T>> register(String id, Supplier<EntityType.Builder<T>> builderSupplier) {
        return ENTITY_TYPES.register(id, () -> builderSupplier.get().build(CraftableComforts.MODID + ":" + id));
    }

}
