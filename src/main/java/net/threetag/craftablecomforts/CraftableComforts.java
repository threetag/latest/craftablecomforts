package net.threetag.craftablecomforts;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.threetag.craftablecomforts.block.CCBlocks;
import net.threetag.craftablecomforts.data.*;
import net.threetag.craftablecomforts.entity.CCEntityTypes;
import net.threetag.craftablecomforts.item.CCItems;
import net.threetag.craftablecomforts.sound.CCSoundEvents;
import net.threetag.craftablecomforts.tileentity.CCTileEntityTypes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(CraftableComforts.MODID)
public class CraftableComforts {

    public static final String MODID = "craftablecomforts";
    private static final Logger LOGGER = LogManager.getLogger();

    public CraftableComforts() {
        FMLJavaModLoadingContext.get().getModEventBus().register(this);

        CCItems.ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
        CCBlocks.BLOCKS.register(FMLJavaModLoadingContext.get().getModEventBus());
        CCEntityTypes.ENTITY_TYPES.register(FMLJavaModLoadingContext.get().getModEventBus());
        CCTileEntityTypes.TILE_ENTITIES.register(FMLJavaModLoadingContext.get().getModEventBus());
        CCSoundEvents.SOUND_EVENTS.register(FMLJavaModLoadingContext.get().getModEventBus());

        MinecraftForge.EVENT_BUS.register(new CCBlocks());
    }

    @OnlyIn(Dist.CLIENT)
    @SubscribeEvent
    public void doClientStuff(final FMLClientSetupEvent event) {
        CCBlocks.initRenderTypes();
        CCEntityTypes.initRenderers();
    }

    @SubscribeEvent
    public void gatherData(GatherDataEvent e) {
        CCBlockTagProvider blockTags = new CCBlockTagProvider(e.getGenerator(), e.getExistingFileHelper());
        e.getGenerator().addProvider(blockTags);
        e.getGenerator().addProvider(new CCItemTagProvider(e.getGenerator(), blockTags, e.getExistingFileHelper()));
        e.getGenerator().addProvider(new CCLangProviders.English(e.getGenerator()));
        e.getGenerator().addProvider(new CCLangProviders.German(e.getGenerator()));
        e.getGenerator().addProvider(new CCLootTableProvider(e.getGenerator()));
        e.getGenerator().addProvider(new CCRecipeProvider(e.getGenerator()));
    }
}
