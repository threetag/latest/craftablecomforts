package net.threetag.craftablecomforts.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.threetag.craftablecomforts.sound.CCSoundEvents;

import javax.annotation.Nullable;
import java.util.Random;

public abstract class AbstractCandleBlock extends Block {

    public static final BooleanProperty LIT = BlockStateProperties.LIT;

    public AbstractCandleBlock(Properties properties) {
        super(properties);
    }

    @OnlyIn(Dist.CLIENT)
    protected abstract Iterable<Vector3d> getParticleOffsets(BlockState state);

    @Override
    public void onProjectileCollision(World worldIn, BlockState state, BlockRayTraceResult hit, ProjectileEntity projectile) {
        if (!worldIn.isRemote && projectile.isBurning() && !(Boolean) state.get(LIT)) {
            setLit(worldIn, state, hit.getPos(), true);
        }
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void animateTick(BlockState stateIn, World worldIn, BlockPos pos, Random rand) {
        if (stateIn.get(LIT)) {
            this.getParticleOffsets(stateIn).forEach((offset) -> {
                spawnCandleParticles(worldIn, offset.add(pos.getX(), pos.getY(), pos.getZ()), rand);
            });
        }
    }

    @OnlyIn(Dist.CLIENT)
    private static void spawnCandleParticles(World world, Vector3d vec3d, Random random) {
        float f = random.nextFloat();
        if (f < 0.3F) {
            world.addParticle(ParticleTypes.SMOKE, vec3d.x, vec3d.y, vec3d.z, 0.0D, 0.0D, 0.0D);
            if (f < 0.17F) {
                world.playSound(vec3d.x + 0.5D, vec3d.y + 0.5D, vec3d.z + 0.5D, CCSoundEvents.BLOCK_CANDLE_AMBIENT.get(), SoundCategory.BLOCKS, 1.0F + random.nextFloat(), random.nextFloat() * 0.7F + 0.3F, false);
            }
        }

        world.addParticle(ParticleTypes.FLAME, vec3d.x, vec3d.y, vec3d.z, 0.0D, 0.0D, 0.0D);
    }

    protected static void extinguish(@Nullable PlayerEntity player, BlockState state, IWorld world, BlockPos pos) {
        setLit(world, state, pos, false);
        world.addParticle(ParticleTypes.SMOKE, pos.getX(), pos.getY(), pos.getZ(), 0.0D, 0.10000000149011612D, 0.0D);
        world.playSound(null, pos, CCSoundEvents.BLOCK_CANDLE_EXTINGUISH.get(), SoundCategory.BLOCKS, 1.0F, 1.0F);
//        world.playEvent(player, GameEvent.BLOCK_CHANGE, pos);
    }

    private static void setLit(IWorld world, BlockState state, BlockPos pos, boolean lit) {
        world.setBlockState(pos, state.with(LIT, lit), 11);
    }
}
