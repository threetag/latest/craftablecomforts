package net.threetag.craftablecomforts.block;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.threetag.craftablecomforts.CraftableComforts;
import net.threetag.craftablecomforts.item.CCItems;
import net.threetag.craftablecomforts.item.ItemGroupFiller;
import net.threetag.craftablecomforts.item.SortedBlockItem;
import net.threetag.craftablecomforts.sound.CCSoundEvents;
import net.threetag.craftablecomforts.tags.CCItemTags;

import java.util.Random;
import java.util.function.Supplier;

public class CCBlocks {

    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, CraftableComforts.MODID);
    public static final ItemGroupFiller FILLER_IRON_BARS = new ItemGroupFiller(() -> Blocks.IRON_BARS);

    public static final RegistryObject<Block> BENCH = register("bench", () -> new BenchBlock(AbstractBlock.Properties.create(Material.IRON).setRequiresTool().hardnessAndResistance(5.0F, 6.0F).sound(SoundType.METAL).notSolid()));

    public static final RegistryObject<Block> OAK_CRATE = register("oak_crate", () -> new CrateBlock(Block.Properties.from(Blocks.OAK_PLANKS)));
    public static final RegistryObject<Block> SPRUCE_CRATE = register("spruce_crate", () -> new CrateBlock(Block.Properties.from(Blocks.SPRUCE_PLANKS)));
    public static final RegistryObject<Block> BIRCH_CRATE = register("birch_crate", () -> new CrateBlock(Block.Properties.from(Blocks.BIRCH_PLANKS)));
    public static final RegistryObject<Block> JUNGLE_CRATE = register("jungle_crate", () -> new CrateBlock(Block.Properties.from(Blocks.JUNGLE_PLANKS)));
    public static final RegistryObject<Block> ACACIA_CRATE = register("acacia_crate", () -> new CrateBlock(Block.Properties.from(Blocks.ACACIA_PLANKS)));
    public static final RegistryObject<Block> DARK_OAK_CRATE = register("dark_oak_crate", () -> new CrateBlock(Block.Properties.from(Blocks.DARK_OAK_PLANKS)));
    public static final RegistryObject<Block> CRIMSON_CRATE = register("crimson_crate", () -> new CrateBlock(Block.Properties.from(Blocks.CRIMSON_PLANKS)));
    public static final RegistryObject<Block> WARPED_CRATE = register("warped_crate", () -> new CrateBlock(Block.Properties.from(Blocks.WARPED_PLANKS)));

    public static final RegistryObject<Block> BLOCK_RAILING = register("block_railing", () -> new CCPaneBlock(AbstractBlock.Properties.from(Blocks.IRON_BARS)), FILLER_IRON_BARS);
    public static final RegistryObject<Block> ORNATE_RAILING = register("ornate_railing", () -> new CCPaneBlock(AbstractBlock.Properties.from(Blocks.IRON_BARS)), FILLER_IRON_BARS);
    public static final RegistryObject<Block> WAVE_RAILING = register("wave_railing", () -> new CCPaneBlock(AbstractBlock.Properties.from(Blocks.IRON_BARS)), FILLER_IRON_BARS);
    public static final RegistryObject<Block> CHAINED_RAILING = register("chained_railing", () -> new CCPaneBlock(AbstractBlock.Properties.from(Blocks.IRON_BARS)), FILLER_IRON_BARS);

    public static final RegistryObject<Block> CANDLE = register("candle", () -> new CandleBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS, MaterialColor.SAND).notSolid().hardnessAndResistance(0.1F).sound(CCSoundEvents.CANDLE).setLightLevel(CandleBlock.STATE_TO_LUMINANCE)));
    public static final RegistryObject<Block> WHITE_CANDLE = register("white_candle", () -> new CandleBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS, MaterialColor.WOOL).notSolid().hardnessAndResistance(0.1F).sound(CCSoundEvents.CANDLE).setLightLevel(CandleBlock.STATE_TO_LUMINANCE)));
    public static final RegistryObject<Block> ORANGE_CANDLE = register("orange_candle", () -> new CandleBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS, MaterialColor.ADOBE).notSolid().hardnessAndResistance(0.1F).sound(CCSoundEvents.CANDLE).setLightLevel(CandleBlock.STATE_TO_LUMINANCE)));
    public static final RegistryObject<Block> MAGENTA_CANDLE = register("magenta_candle", () -> new CandleBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS, MaterialColor.MAGENTA).notSolid().hardnessAndResistance(0.1F).sound(CCSoundEvents.CANDLE).setLightLevel(CandleBlock.STATE_TO_LUMINANCE)));
    public static final RegistryObject<Block> LIGHT_BLUE_CANDLE = register("light_blue_candle", () -> new CandleBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS, MaterialColor.LIGHT_BLUE).notSolid().hardnessAndResistance(0.1F).sound(CCSoundEvents.CANDLE).setLightLevel(CandleBlock.STATE_TO_LUMINANCE)));
    public static final RegistryObject<Block> YELLOW_CANDLE = register("yellow_candle", () -> new CandleBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS, MaterialColor.YELLOW).notSolid().hardnessAndResistance(0.1F).sound(CCSoundEvents.CANDLE).setLightLevel(CandleBlock.STATE_TO_LUMINANCE)));
    public static final RegistryObject<Block> LIME_CANDLE = register("lime_candle", () -> new CandleBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS, MaterialColor.LIME).notSolid().hardnessAndResistance(0.1F).sound(CCSoundEvents.CANDLE).setLightLevel(CandleBlock.STATE_TO_LUMINANCE)));
    public static final RegistryObject<Block> PINK_CANDLE = register("pink_candle", () -> new CandleBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS, MaterialColor.PINK).notSolid().hardnessAndResistance(0.1F).sound(CCSoundEvents.CANDLE).setLightLevel(CandleBlock.STATE_TO_LUMINANCE)));
    public static final RegistryObject<Block> GRAY_CANDLE = register("gray_candle", () -> new CandleBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS, MaterialColor.GRAY).notSolid().hardnessAndResistance(0.1F).sound(CCSoundEvents.CANDLE).setLightLevel(CandleBlock.STATE_TO_LUMINANCE)));
    public static final RegistryObject<Block> LIGHT_GRAY_CANDLE = register("light_gray_candle", () -> new CandleBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS, MaterialColor.LIGHT_GRAY).notSolid().hardnessAndResistance(0.1F).sound(CCSoundEvents.CANDLE).setLightLevel(CandleBlock.STATE_TO_LUMINANCE)));
    public static final RegistryObject<Block> CYAN_CANDLE = register("cyan_candle", () -> new CandleBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS, MaterialColor.CYAN).notSolid().hardnessAndResistance(0.1F).sound(CCSoundEvents.CANDLE).setLightLevel(CandleBlock.STATE_TO_LUMINANCE)));
    public static final RegistryObject<Block> PURPLE_CANDLE = register("purple_candle", () -> new CandleBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS, MaterialColor.PURPLE).notSolid().hardnessAndResistance(0.1F).sound(CCSoundEvents.CANDLE).setLightLevel(CandleBlock.STATE_TO_LUMINANCE)));
    public static final RegistryObject<Block> BLUE_CANDLE = register("blue_candle", () -> new CandleBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS, MaterialColor.BLUE).notSolid().hardnessAndResistance(0.1F).sound(CCSoundEvents.CANDLE).setLightLevel(CandleBlock.STATE_TO_LUMINANCE)));
    public static final RegistryObject<Block> BROWN_CANDLE = register("brown_candle", () -> new CandleBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS, MaterialColor.BROWN).notSolid().hardnessAndResistance(0.1F).sound(CCSoundEvents.CANDLE).setLightLevel(CandleBlock.STATE_TO_LUMINANCE)));
    public static final RegistryObject<Block> GREEN_CANDLE = register("green_candle", () -> new CandleBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS, MaterialColor.GREEN).notSolid().hardnessAndResistance(0.1F).sound(CCSoundEvents.CANDLE).setLightLevel(CandleBlock.STATE_TO_LUMINANCE)));
    public static final RegistryObject<Block> RED_CANDLE = register("red_candle", () -> new CandleBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS, MaterialColor.RED).notSolid().hardnessAndResistance(0.1F).sound(CCSoundEvents.CANDLE).setLightLevel(CandleBlock.STATE_TO_LUMINANCE)));
    public static final RegistryObject<Block> BLACK_CANDLE = register("black_candle", () -> new CandleBlock(AbstractBlock.Properties.create(Material.MISCELLANEOUS, MaterialColor.BLACK).notSolid().hardnessAndResistance(0.1F).sound(CCSoundEvents.CANDLE).setLightLevel(CandleBlock.STATE_TO_LUMINANCE)));
    public static final RegistryObject<Block> CANDLE_CAKE = register("candle_cake", () -> new CandleCakeBlock(CANDLE, AbstractBlock.Properties.from(Blocks.CAKE).setLightLevel((state) -> (Boolean) state.get(AbstractCandleBlock.LIT) ? 3 : 0)));
    public static final RegistryObject<Block> WHITE_CANDLE_CAKE = BLOCKS.register("white_candle_cake", () -> new CandleCakeBlock(WHITE_CANDLE, AbstractBlock.Properties.from(CANDLE_CAKE.get())));
    public static final RegistryObject<Block> ORANGE_CANDLE_CAKE = BLOCKS.register("orange_candle_cake", () -> new CandleCakeBlock(ORANGE_CANDLE, AbstractBlock.Properties.from(CANDLE_CAKE.get())));
    public static final RegistryObject<Block> MAGENTA_CANDLE_CAKE = BLOCKS.register("magenta_candle_cake", () -> new CandleCakeBlock(MAGENTA_CANDLE, AbstractBlock.Properties.from(CANDLE_CAKE.get())));
    public static final RegistryObject<Block> LIGHT_BLUE_CANDLE_CAKE = BLOCKS.register("light_blue_candle_cake", () -> new CandleCakeBlock(LIGHT_BLUE_CANDLE, AbstractBlock.Properties.from(CANDLE_CAKE.get())));
    public static final RegistryObject<Block> YELLOW_CANDLE_CAKE = BLOCKS.register("yellow_candle_cake", () -> new CandleCakeBlock(YELLOW_CANDLE, AbstractBlock.Properties.from(CANDLE_CAKE.get())));
    public static final RegistryObject<Block> LIME_CANDLE_CAKE = BLOCKS.register("lime_candle_cake", () -> new CandleCakeBlock(LIME_CANDLE, AbstractBlock.Properties.from(CANDLE_CAKE.get())));
    public static final RegistryObject<Block> PINK_CANDLE_CAKE = BLOCKS.register("pink_candle_cake", () -> new CandleCakeBlock(PINK_CANDLE, AbstractBlock.Properties.from(CANDLE_CAKE.get())));
    public static final RegistryObject<Block> GRAY_CANDLE_CAKE = BLOCKS.register("gray_candle_cake", () -> new CandleCakeBlock(GRAY_CANDLE, AbstractBlock.Properties.from(CANDLE_CAKE.get())));
    public static final RegistryObject<Block> LIGHT_GRAY_CANDLE_CAKE = BLOCKS.register("light_gray_candle_cake", () -> new CandleCakeBlock(LIGHT_GRAY_CANDLE, AbstractBlock.Properties.from(CANDLE_CAKE.get())));
    public static final RegistryObject<Block> CYAN_CANDLE_CAKE = BLOCKS.register("cyan_candle_cake", () -> new CandleCakeBlock(CYAN_CANDLE, AbstractBlock.Properties.from(CANDLE_CAKE.get())));
    public static final RegistryObject<Block> PURPLE_CANDLE_CAKE = BLOCKS.register("purple_candle_cake", () -> new CandleCakeBlock(PURPLE_CANDLE, AbstractBlock.Properties.from(CANDLE_CAKE.get())));
    public static final RegistryObject<Block> BLUE_CANDLE_CAKE = BLOCKS.register("blue_candle_cake", () -> new CandleCakeBlock(BLUE_CANDLE, AbstractBlock.Properties.from(CANDLE_CAKE.get())));
    public static final RegistryObject<Block> BROWN_CANDLE_CAKE = BLOCKS.register("brown_candle_cake", () -> new CandleCakeBlock(BROWN_CANDLE, AbstractBlock.Properties.from(CANDLE_CAKE.get())));
    public static final RegistryObject<Block> GREEN_CANDLE_CAKE = BLOCKS.register("green_candle_cake", () -> new CandleCakeBlock(GREEN_CANDLE, AbstractBlock.Properties.from(CANDLE_CAKE.get())));
    public static final RegistryObject<Block> RED_CANDLE_CAKE = BLOCKS.register("red_candle_cake", () -> new CandleCakeBlock(RED_CANDLE, AbstractBlock.Properties.from(CANDLE_CAKE.get())));
    public static final RegistryObject<Block> BLACK_CANDLE_CAKE = BLOCKS.register("black_candle_cake", () -> new CandleCakeBlock(BLACK_CANDLE, AbstractBlock.Properties.from(CANDLE_CAKE.get())));

    @OnlyIn(Dist.CLIENT)
    public static void initRenderTypes() {
        RenderTypeLookup.setRenderLayer(BENCH.get(), RenderType.getCutoutMipped());
        RenderTypeLookup.setRenderLayer(BLOCK_RAILING.get(), RenderType.getCutoutMipped());
        RenderTypeLookup.setRenderLayer(ORNATE_RAILING.get(), RenderType.getCutoutMipped());
        RenderTypeLookup.setRenderLayer(WAVE_RAILING.get(), RenderType.getCutoutMipped());
        RenderTypeLookup.setRenderLayer(CHAINED_RAILING.get(), RenderType.getCutoutMipped());
    }

    @SubscribeEvent
    public void fixMappings(RegistryEvent.MissingMappings<Block> e) {
        e.getMappings("craftfallessentials").forEach(mapping -> {
            Block block = ForgeRegistries.BLOCKS.getValue(new ResourceLocation(CraftableComforts.MODID, mapping.key.getPath()));

            if (block != null) {
                mapping.remap(block);
            }
        });
    }

    @SubscribeEvent
    public void onItemRightClick(PlayerInteractEvent.RightClickBlock e) {
        PlayerEntity playerEntity = e.getPlayer();
        World world = e.getWorld();
        BlockPos blockPos = e.getPos();
        BlockState blockState = world.getBlockState(blockPos);
        if (e.getItemStack().getItem() instanceof FlintAndSteelItem || e.getItemStack().getItem() instanceof FireChargeItem) {

            if (CandleBlock.canBeLit(blockState) || CandleCakeBlock.canBeLit(blockState)) {
                world.playSound(playerEntity, blockPos, SoundEvents.ITEM_FLINTANDSTEEL_USE, SoundCategory.BLOCKS, 1.0F, new Random().nextFloat() * 0.4F + 0.8F);
                world.setBlockState(blockPos, blockState.with(BlockStateProperties.LIT, Boolean.TRUE), 11);

                if (e.getItemStack().getItem() instanceof FlintAndSteelItem) {
                    if (playerEntity != null) {
                        e.getItemStack().damageItem(1, playerEntity, (player) -> {
                            player.sendBreakAnimation(e.getHand());
                        });
                    }
                } else {
                    e.getItemStack().shrink(1);
                }

                e.setCancellationResult(ActionResultType.func_233537_a_(world.isRemote()));
                e.setCanceled(true);
            }
        } else if (e.getItemStack().getItem().isIn(CCItemTags.CANDLES) && blockState.getBlock() == Blocks.CAKE && blockState.get(CakeBlock.BITES) == 0) {
            Block block = Block.getBlockFromItem(e.getItemStack().getItem());
            if (block instanceof CandleBlock) {
                if (!playerEntity.isCreative()) {
                    e.getItemStack().shrink(1);
                }

                world.playSound(null, e.getPos(), CCSoundEvents.BLOCK_CAKE_ADD_CANDLE.get(), SoundCategory.BLOCKS, 1.0F, 1.0F);
                world.setBlockState(e.getPos(), CandleCakeBlock.getCandleCakeFromCandle(block));
                e.setCancellationResult(ActionResultType.SUCCESS);
                e.setCanceled(true);
            }
        }
    }

    public static <T extends Block> RegistryObject<T> register(String id, Supplier<T> blockSupplier) {
        RegistryObject<T> registryObject = BLOCKS.register(id, blockSupplier);
        CCItems.ITEMS.register(id, () -> new BlockItem(registryObject.get(), new Item.Properties().group(ItemGroup.DECORATIONS)));
        return registryObject;
    }

    public static <T extends Block> RegistryObject<T> register(String id, Supplier<T> blockSupplier, ItemGroupFiller filler) {
        RegistryObject<T> registryObject = BLOCKS.register(id, blockSupplier);
        CCItems.ITEMS.register(id, () -> new SortedBlockItem(registryObject.get(), filler, new Item.Properties().group(ItemGroup.DECORATIONS)));
        return registryObject;
    }

    public static <T extends Block> RegistryObject<T> register(String id, Supplier<T> blockSupplier, Supplier<Item> itemSupplier) {
        RegistryObject<T> registryObject = BLOCKS.register(id, blockSupplier);
        CCItems.ITEMS.register(id, itemSupplier);
        return registryObject;
    }

    public static <T extends Block> RegistryObject<T> register(String id, Supplier<T> blockSupplier, ItemGroup itemGroup) {
        RegistryObject<T> registryObject = BLOCKS.register(id, blockSupplier);
        CCItems.ITEMS.register(id, () -> new BlockItem(registryObject.get(), new Item.Properties().group(itemGroup)));
        return registryObject;
    }

    public static <T extends Block> RegistryObject<T> register(String id, Supplier<T> blockSupplier, ItemGroup itemGroup, ItemGroupFiller filler) {
        RegistryObject<T> registryObject = BLOCKS.register(id, blockSupplier);
        CCItems.ITEMS.register(id, () -> new SortedBlockItem(registryObject.get(), filler, new Item.Properties().group(itemGroup)));
        return registryObject;
    }

    public static <T extends Block> RegistryObject<T> register(String id, Supplier<T> blockSupplier, Rarity rarity) {
        RegistryObject<T> registryObject = BLOCKS.register(id, blockSupplier);
        CCItems.ITEMS.register(id, () -> new BlockItem(registryObject.get(), new Item.Properties().group(ItemGroup.BUILDING_BLOCKS).rarity(rarity)));
        return registryObject;
    }

    public static <T extends Block> RegistryObject<T> register(String id, Supplier<T> blockSupplier, Rarity rarity, ItemGroupFiller filler) {
        RegistryObject<T> registryObject = BLOCKS.register(id, blockSupplier);
        CCItems.ITEMS.register(id, () -> new SortedBlockItem(registryObject.get(), filler, new Item.Properties().group(ItemGroup.BUILDING_BLOCKS).rarity(rarity)));
        return registryObject;
    }

}
