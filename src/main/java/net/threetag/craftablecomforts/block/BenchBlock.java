package net.threetag.craftablecomforts.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.HorizontalBlock;
import net.minecraft.block.IWaterLoggable;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.EnumProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.threetag.craftablecomforts.entity.SitableEntity;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class BenchBlock extends HorizontalBlock implements IWaterLoggable, ISitableBlock {

    public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;
    public static final EnumProperty<BenchPart> BENCH_PART = EnumProperty.create("bench_part", BenchPart.class);
    private static final Map<Integer, VoxelShape> SHAPES = new HashMap<>();

    public BenchBlock(Properties builder) {
        super(builder);
        this.setDefaultState(this.getDefaultState().with(WATERLOGGED, false).with(BENCH_PART, BenchPart.SINGLE).with(HORIZONTAL_FACING, Direction.NORTH));
    }

    @Override
    public float getSitOffset(BlockState state, BlockPos pos) {
        return 0.2F;
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (worldIn.isRemote) {
            return ActionResultType.SUCCESS;
        } else {
            SitableEntity sitableEntity = new SitableEntity(worldIn, pos, player);
            worldIn.addEntity(sitableEntity);
            return ActionResultType.CONSUME;
        }
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        int i = state.get(HORIZONTAL_FACING).ordinal() * 10 + state.get(BENCH_PART).ordinal();
        return SHAPES.get(i);
    }

    @Override
    public FluidState getFluidState(BlockState state) {
        return state.get(WATERLOGGED) ? Fluids.WATER.getStillFluidState(false) : super.getFluidState(state);
    }

    @Override
    public BlockState updatePostPlacement(BlockState stateIn, Direction facing, BlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos) {
        if (stateIn.get(WATERLOGGED)) {
            worldIn.getPendingFluidTicks().scheduleTick(currentPos, Fluids.WATER, Fluids.WATER.getTickRate(worldIn));
        }

        return super.updatePostPlacement(stateIn, facing, facingState, worldIn, currentPos, facingPos);
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(WATERLOGGED, HORIZONTAL_FACING, BENCH_PART);
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        FluidState fluidstate = context.getWorld().getFluidState(context.getPos());
        Direction facing = context.getPlacementHorizontalFacing().getOpposite();
        BlockState left = context.getWorld().getBlockState(context.getPos().add(facing.rotateY().getDirectionVec()));
        BlockState right = context.getWorld().getBlockState(context.getPos().add(facing.rotateYCCW().getDirectionVec()));
        boolean hasLeft = left.getBlock() instanceof BenchBlock && left.get(HORIZONTAL_FACING) == facing;
        boolean hasRight = right.getBlock() instanceof BenchBlock && right.get(HORIZONTAL_FACING) == facing;
        BenchPart part = hasLeft && hasRight ? BenchPart.MIDDLE : hasLeft ? BenchPart.RIGHT : hasRight ? BenchPart.LEFT : BenchPart.SINGLE;

        return this.getDefaultState().with(HORIZONTAL_FACING, facing).with(WATERLOGGED, fluidstate.getFluid() == Fluids.WATER).with(BENCH_PART, part);
    }

    @Override
    public void neighborChanged(BlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving) {
        Direction facing = state.get(HORIZONTAL_FACING);
        BlockState left = worldIn.getBlockState(pos.add(facing.rotateY().getDirectionVec()));
        BlockState right = worldIn.getBlockState(pos.add(facing.rotateYCCW().getDirectionVec()));
        boolean hasLeft = left.getBlock() instanceof BenchBlock && left.get(HORIZONTAL_FACING) == facing;
        boolean hasRight = right.getBlock() instanceof BenchBlock && right.get(HORIZONTAL_FACING) == facing;
        BenchPart part = hasLeft && hasRight ? BenchPart.MIDDLE : hasLeft ? BenchPart.RIGHT : hasRight ? BenchPart.LEFT : BenchPart.SINGLE;

        worldIn.setBlockState(pos, state.with(BENCH_PART, part));
        super.neighborChanged(state, worldIn, pos, blockIn, fromPos, isMoving);
    }

    public enum BenchPart implements IStringSerializable {

        SINGLE("single"),
        LEFT("left"),
        MIDDLE("middle"),
        RIGHT("right");

        private final String name;

        BenchPart(String name) {
            this.name = name;
        }

        @Override
        public String getString() {
            return this.name;
        }
    }

    static {
        for (Direction direction : Direction.values()) {
            if (direction.getAxis() != Direction.Axis.Y) {
                for (BenchPart part : BenchPart.values()) {
                    int i = direction.ordinal() * 10 + part.ordinal();
                    VoxelShape shape;

                    if (part == BenchPart.SINGLE) {
                        switch (direction) {
                            case NORTH:
                                shape = Stream.of(
                                        Block.makeCuboidShape(0, 0, 1, 2, 8, 3),
                                        Block.makeCuboidShape(0, 0, 13, 2, 8, 15),
                                        Block.makeCuboidShape(14, 0, 13, 16, 8, 15),
                                        Block.makeCuboidShape(14, 0, 1, 16, 8, 3),
                                        Block.makeCuboidShape(0, 8, 0, 2, 10, 16),
                                        Block.makeCuboidShape(14, 8, 0, 16, 10, 16),
                                        Block.makeCuboidShape(0, 10, 14, 2, 23, 16),
                                        Block.makeCuboidShape(14, 10, 14, 16, 23, 16),
                                        Block.makeCuboidShape(2, 8, 0, 14, 9, 15),
                                        Block.makeCuboidShape(14, 10, 0, 16, 16, 14),
                                        Block.makeCuboidShape(2, 8, 15, 14, 23, 16),
                                        Block.makeCuboidShape(0, 10, 0, 2, 16, 14)
                                ).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();
                                break;
                            case EAST:
                                shape = Stream.of(
                                        Block.makeCuboidShape(13, 0, 0, 15, 8, 2),
                                        Block.makeCuboidShape(1, 0, 0, 3, 8, 2),
                                        Block.makeCuboidShape(1, 0, 14, 3, 8, 16),
                                        Block.makeCuboidShape(13, 0, 14, 15, 8, 16),
                                        Block.makeCuboidShape(0, 8, 0, 16, 10, 2),
                                        Block.makeCuboidShape(0, 8, 14, 16, 10, 16),
                                        Block.makeCuboidShape(0, 10, 0, 2, 23, 2),
                                        Block.makeCuboidShape(0, 10, 14, 2, 23, 16),
                                        Block.makeCuboidShape(1, 8, 2, 16, 9, 14),
                                        Block.makeCuboidShape(2, 10, 14, 16, 16, 16),
                                        Block.makeCuboidShape(0, 8, 2, 1, 23, 14),
                                        Block.makeCuboidShape(2, 10, 0, 16, 16, 2)
                                ).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();
                                break;
                            case SOUTH:
                                shape = Stream.of(
                                        Block.makeCuboidShape(14, 0, 13, 16, 8, 15),
                                        Block.makeCuboidShape(14, 0, 1, 16, 8, 3),
                                        Block.makeCuboidShape(0, 0, 1, 2, 8, 3),
                                        Block.makeCuboidShape(0, 0, 13, 2, 8, 15),
                                        Block.makeCuboidShape(14, 8, 0, 16, 10, 16),
                                        Block.makeCuboidShape(0, 8, 0, 2, 10, 16),
                                        Block.makeCuboidShape(14, 10, 0, 16, 23, 2),
                                        Block.makeCuboidShape(0, 10, 0, 2, 23, 2),
                                        Block.makeCuboidShape(2, 8, 1, 14, 9, 16),
                                        Block.makeCuboidShape(0, 10, 2, 2, 16, 16),
                                        Block.makeCuboidShape(2, 8, 0, 14, 23, 1),
                                        Block.makeCuboidShape(14, 10, 2, 16, 16, 16)
                                ).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();
                                break;
                            default:
                                shape = Stream.of(
                                        Block.makeCuboidShape(1, 0, 14, 3, 8, 16),
                                        Block.makeCuboidShape(13, 0, 14, 15, 8, 16),
                                        Block.makeCuboidShape(13, 0, 0, 15, 8, 2),
                                        Block.makeCuboidShape(1, 0, 0, 3, 8, 2),
                                        Block.makeCuboidShape(0, 8, 14, 16, 10, 16),
                                        Block.makeCuboidShape(0, 8, 0, 16, 10, 2),
                                        Block.makeCuboidShape(14, 10, 14, 16, 23, 16),
                                        Block.makeCuboidShape(14, 10, 0, 16, 23, 2),
                                        Block.makeCuboidShape(0, 8, 2, 15, 9, 14),
                                        Block.makeCuboidShape(0, 10, 0, 14, 16, 2),
                                        Block.makeCuboidShape(15, 8, 2, 16, 23, 14),
                                        Block.makeCuboidShape(0, 10, 14, 14, 16, 16)
                                ).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();
                                break;
                        }

                    } else if (part == BenchPart.LEFT) {
                        switch (direction) {
                            case NORTH:
                                shape = Stream.of(
                                        Block.makeCuboidShape(14, 0, 1, 16, 8, 3),
                                        Block.makeCuboidShape(14, 0, 13, 16, 8, 15),
                                        Block.makeCuboidShape(14, 8, 0, 16, 10, 16),
                                        Block.makeCuboidShape(14, 10, 14, 16, 23, 16),
                                        Block.makeCuboidShape(0, 8, 15, 14, 23, 16),
                                        Block.makeCuboidShape(0, 8, 0, 14, 9, 15),
                                        Block.makeCuboidShape(14, 10, 0, 16, 16, 14)
                                ).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();
                                break;
                            case EAST:
                                shape = Stream.of(
                                        Block.makeCuboidShape(13, 0, 14, 15, 8, 16),
                                        Block.makeCuboidShape(1, 0, 14, 3, 8, 16),
                                        Block.makeCuboidShape(0, 8, 14, 16, 10, 16),
                                        Block.makeCuboidShape(0, 10, 14, 2, 23, 16),
                                        Block.makeCuboidShape(0, 8, 0, 1, 23, 14),
                                        Block.makeCuboidShape(1, 8, 0, 16, 9, 14),
                                        Block.makeCuboidShape(2, 10, 14, 16, 16, 16)
                                ).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();
                                break;
                            case SOUTH:
                                shape = Stream.of(
                                        Block.makeCuboidShape(0, 0, 13, 2, 8, 15),
                                        Block.makeCuboidShape(0, 0, 1, 2, 8, 3),
                                        Block.makeCuboidShape(0, 8, 0, 2, 10, 16),
                                        Block.makeCuboidShape(0, 10, 0, 2, 23, 2),
                                        Block.makeCuboidShape(2, 8, 0, 16, 23, 1),
                                        Block.makeCuboidShape(2, 8, 1, 16, 9, 16),
                                        Block.makeCuboidShape(0, 10, 2, 2, 16, 16)
                                ).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();
                                break;
                            default:
                                shape = Stream.of(
                                        Block.makeCuboidShape(1, 0, 0, 3, 8, 2),
                                        Block.makeCuboidShape(13, 0, 0, 15, 8, 2),
                                        Block.makeCuboidShape(0, 8, 0, 16, 10, 2),
                                        Block.makeCuboidShape(14, 10, 0, 16, 23, 2),
                                        Block.makeCuboidShape(15, 8, 2, 16, 23, 16),
                                        Block.makeCuboidShape(0, 8, 2, 15, 9, 16),
                                        Block.makeCuboidShape(0, 10, 0, 14, 16, 2)
                                ).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();
                                break;
                        }
                    } else if (part == BenchPart.MIDDLE) {
                        switch (direction) {
                            case NORTH:
                                shape = VoxelShapes.combineAndSimplify(Block.makeCuboidShape(0, 8, 0, 16, 9, 15), Block.makeCuboidShape(0, 8, 15, 16, 23, 16), IBooleanFunction.OR);
                                break;
                            case EAST:
                                shape = VoxelShapes.combineAndSimplify(Block.makeCuboidShape(1, 8, 0, 16, 9, 16), Block.makeCuboidShape(0, 8, 0, 1, 23, 16), IBooleanFunction.OR);
                                break;
                            case SOUTH:
                                shape = VoxelShapes.combineAndSimplify(Block.makeCuboidShape(0, 8, 1, 16, 9, 16), Block.makeCuboidShape(0, 8, 0, 16, 23, 1), IBooleanFunction.OR);
                                break;
                            default:
                                shape = VoxelShapes.combineAndSimplify(Block.makeCuboidShape(0, 8, 0, 15, 9, 16), Block.makeCuboidShape(15, 8, 0, 16, 23, 16), IBooleanFunction.OR);
                                break;
                        }
                    } else {
                        switch (direction) {
                            case NORTH:
                                shape = Stream.of(
                                        Block.makeCuboidShape(0, 0, 1, 2, 8, 3),
                                        Block.makeCuboidShape(0, 0, 13, 2, 8, 15),
                                        Block.makeCuboidShape(0, 8, 0, 2, 10, 16),
                                        Block.makeCuboidShape(0, 10, 0, 2, 16, 14),
                                        Block.makeCuboidShape(0, 10, 14, 2, 23, 16),
                                        Block.makeCuboidShape(2, 8, 0, 16, 9, 15),
                                        Block.makeCuboidShape(2, 8, 15, 16, 23, 16)
                                ).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();
                                break;
                            case EAST:
                                shape = Stream.of(
                                        Block.makeCuboidShape(13, 0, 0, 15, 8, 2),
                                        Block.makeCuboidShape(1, 0, 0, 3, 8, 2),
                                        Block.makeCuboidShape(0, 8, 0, 16, 10, 2),
                                        Block.makeCuboidShape(2, 10, 0, 16, 16, 2),
                                        Block.makeCuboidShape(0, 10, 0, 2, 23, 2),
                                        Block.makeCuboidShape(1, 8, 2, 16, 9, 16),
                                        Block.makeCuboidShape(0, 8, 2, 1, 23, 16)
                                ).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();
                                break;
                            case SOUTH:
                                shape = Stream.of(
                                        Block.makeCuboidShape(14, 0, 13, 16, 8, 15),
                                        Block.makeCuboidShape(14, 0, 1, 16, 8, 3),
                                        Block.makeCuboidShape(14, 8, 0, 16, 10, 16),
                                        Block.makeCuboidShape(14, 10, 2, 16, 16, 16),
                                        Block.makeCuboidShape(14, 10, 0, 16, 23, 2),
                                        Block.makeCuboidShape(0, 8, 1, 14, 9, 16),
                                        Block.makeCuboidShape(0, 8, 0, 14, 23, 1)
                                ).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();
                                break;
                            default:
                                shape = Stream.of(
                                        Block.makeCuboidShape(1, 0, 14, 3, 8, 16),
                                        Block.makeCuboidShape(13, 0, 14, 15, 8, 16),
                                        Block.makeCuboidShape(0, 8, 14, 16, 10, 16),
                                        Block.makeCuboidShape(0, 10, 14, 14, 16, 16),
                                        Block.makeCuboidShape(14, 10, 14, 16, 23, 16),
                                        Block.makeCuboidShape(0, 8, 0, 15, 9, 14),
                                        Block.makeCuboidShape(15, 8, 0, 16, 23, 14)
                                ).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();
                                break;
                        }
                    }

                    SHAPES.put(i, shape);
                }
            }
        }
    }

}
