package net.threetag.craftablecomforts.block;

import com.google.common.collect.ImmutableList;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.CakeBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.pathfinding.PathType;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.stats.Stats;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.RegistryObject;
import net.threetag.craftablecomforts.tags.CCBlockTags;

import java.util.HashMap;
import java.util.Map;

public class CandleCakeBlock extends AbstractCandleBlock {

    public static final BooleanProperty LIT = AbstractCandleBlock.LIT;
    protected static final VoxelShape CAKE_SHAPE = VoxelShapes.create(1.0D / 16D, 0.0D, 1.0D / 16D, 15.0D / 16D, 8.0D / 16D, 15.0D / 16D);
    protected static final VoxelShape CANDLE_SHAPE = VoxelShapes.create(7.0D / 16D, 8.0D / 16D, 7.0D / 16D, 9.0D / 16D, 14.0D / 16D, 9.0D / 16D);
    protected static final VoxelShape SHAPE = VoxelShapes.or(CAKE_SHAPE, CANDLE_SHAPE);
    private static final Map<ResourceLocation, CandleCakeBlock> CANDLES_TO_CANDLE_CAKES = new HashMap<>();
    private static final Iterable<Vector3d> PARTICLE_OFFSETS = ImmutableList.of(new Vector3d(0.5D, 1.0D, 0.5D));

    public CandleCakeBlock(RegistryObject<Block> candle, Properties properties) {
        super(properties);
        this.setDefaultState(this.getDefaultState().with(LIT, false));
        CANDLES_TO_CANDLE_CAKES.put(candle.getId(), this);
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    protected Iterable<Vector3d> getParticleOffsets(BlockState state) {
        return PARTICLE_OFFSETS;
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPE;
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        ItemStack itemStack = player.getHeldItem(handIn);
        if (itemStack.getItem() != Items.FLINT_AND_STEEL && itemStack.getItem() != Items.FIRE_CHARGE) {
            if (isHittingCandle(hit) && player.getHeldItem(handIn).isEmpty() && state.get(LIT)) {
                extinguish(player, state, worldIn, pos);
                return ActionResultType.func_233537_a_(worldIn.isRemote);
            } else {
                ActionResultType actionResult = eatSlice(worldIn, pos, Blocks.CAKE.getDefaultState(), player);
                if (actionResult.isSuccess()) {
                    spawnDrops(state, worldIn, pos);
                }

                return actionResult;
            }
        } else {
            return ActionResultType.PASS;
        }
    }

    private static boolean isHittingCandle(BlockRayTraceResult hitResult) {
        return hitResult.getHitVec().getY() - (double) hitResult.getPos().getY() > 0.5D;
    }

    private ActionResultType eatSlice(IWorld world, BlockPos pos, BlockState state, PlayerEntity player) {
        if (!player.canEat(false)) {
            return ActionResultType.PASS;
        } else {
            player.addStat(Stats.EAT_CAKE_SLICE);
            player.getFoodStats().addStats(2, 0.1F);
            int i = state.get(CakeBlock.BITES);
            if (i < 6) {
                world.setBlockState(pos, state.with(CakeBlock.BITES, i + 1), 3);
            } else {
                world.removeBlock(pos, false);
            }

            return ActionResultType.SUCCESS;
        }
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(LIT);
    }

    @Override
    public ItemStack getItem(IBlockReader worldIn, BlockPos pos, BlockState state) {
        return new ItemStack(Blocks.CAKE);
    }

    @Override
    public BlockState updatePostPlacement(BlockState stateIn, Direction facing, BlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos) {
        return facing == Direction.DOWN && !stateIn.isValidPosition(worldIn, currentPos) ? Blocks.AIR.getDefaultState() : super.updatePostPlacement(stateIn, facing, facingState, worldIn, currentPos, facingPos);
    }

    @Override
    public boolean isValidPosition(BlockState state, IWorldReader worldIn, BlockPos pos) {
        return worldIn.getBlockState(pos.down()).getMaterial().isSolid();
    }

    @Override
    public int getComparatorInputOverride(BlockState blockState, World worldIn, BlockPos pos) {
        return 0;
    }

    @Override
    public boolean hasComparatorInputOverride(BlockState state) {
        return true;
    }

    @Override
    public boolean allowsMovement(BlockState state, IBlockReader worldIn, BlockPos pos, PathType type) {
        return false;
    }

    public static BlockState getCandleCakeFromCandle(Block candle) {
        return CANDLES_TO_CANDLE_CAKES.get(candle.getRegistryName()).getDefaultState();
    }

    public static boolean canBeLit(BlockState state) {
        return state.isInAndMatches(CCBlockTags.CANDLE_CAKES, (statex) -> {
            return statex.hasProperty(LIT) && !(Boolean) state.get(LIT);
        });
    }
}
