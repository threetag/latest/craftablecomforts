package net.threetag.craftablecomforts.block;

import com.google.common.collect.ImmutableList;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectMaps;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.IWaterLoggable;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.threetag.craftablecomforts.tags.CCBlockTags;

import javax.annotation.Nullable;
import java.util.List;
import java.util.function.ToIntFunction;

public class CandleBlock extends AbstractCandleBlock implements IWaterLoggable {

    public static final IntegerProperty CANDLES = IntegerProperty.create("candles", 1, 4);
    public static final BooleanProperty LIT = AbstractCandleBlock.LIT;
    public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;
    public static final ToIntFunction<BlockState> STATE_TO_LUMINANCE = (state) -> state.get(LIT) ? 3 * state.get(CANDLES) : 0;
    private static final Int2ObjectMap CANDLES_TO_PARTICLE_OFFSETS = Util.make(() -> {
        Int2ObjectMap<List<Vector3d>> int2ObjectMap = new Int2ObjectOpenHashMap();
        int2ObjectMap.defaultReturnValue(ImmutableList.of());
        int2ObjectMap.put(1, ImmutableList.of(new Vector3d(0.5D, 0.5D, 0.5D)));
        int2ObjectMap.put(2, ImmutableList.of(new Vector3d(0.375D, 0.44D, 0.5D), new Vector3d(0.625D, 0.5D, 0.44D)));
        int2ObjectMap.put(3, ImmutableList.of(new Vector3d(0.5D, 0.313D, 0.625D), new Vector3d(0.375D, 0.44D, 0.5D), new Vector3d(0.56D, 0.5D, 0.44D)));
        int2ObjectMap.put(4, ImmutableList.of(new Vector3d(0.44D, 0.313D, 0.56D), new Vector3d(0.625D, 0.44D, 0.56D), new Vector3d(0.375D, 0.44D, 0.375D), new Vector3d(0.56D, 0.5D, 0.375D)));
        return Int2ObjectMaps.unmodifiable(int2ObjectMap);
    });
    private static final VoxelShape ONE_CANDLE_SHAPE = VoxelShapes.create(7.0D / 16D, 0.0D, 7.0D / 16D, 9.0D / 16D, 6.0D / 16D, 9.0D / 16D);
    private static final VoxelShape TWO_CANDLES_SHAPE = VoxelShapes.create(5.0D / 16D, 0.0D, 6.0D / 16D, 11.0D / 16D, 6.0D / 16D, 9.0D / 16D);
    private static final VoxelShape THREE_CANDLES_SHAPE = VoxelShapes.create(5.0D / 16D, 0.0D, 6.0D / 16D, 10.0D / 16D, 6.0D / 16D, 11.0D / 16D);
    private static final VoxelShape FOUR_CANDLES_SHAPE = VoxelShapes.create(5.0D / 16D, 0.0D, 5.0D / 16D, 11.0D / 16D, 6.0D / 16D, 10.0D / 16D);

    public CandleBlock(Properties properties) {
        super(properties);
        this.setDefaultState(this.getDefaultState().with(CANDLES, 1).with(LIT, false).with(WATERLOGGED, false));
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (player.getHeldItem(handIn).isEmpty() && state.get(LIT)) {
            extinguish(player, state, worldIn, pos);
            return ActionResultType.func_233537_a_(worldIn.isRemote);
        } else {
            return ActionResultType.PASS;
        }
    }

    @Override
    public boolean isReplaceable(BlockState state, BlockItemUseContext context) {
        return !context.hasSecondaryUseForPlayer() && context.getItem().getItem() == this.asItem() && state.get(CANDLES) < 4 || super.isReplaceable(state, context);
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        BlockState blockState = context.getWorld().getBlockState(context.getPos());
        if (blockState.isIn(this)) {
            return blockState.func_235896_a_(CANDLES);
        } else {
            FluidState fluidState = context.getWorld().getFluidState(context.getPos());
            boolean bl = fluidState.getFluid() == Fluids.WATER;
            return super.getStateForPlacement(context).with(WATERLOGGED, bl);
        }
    }

    @Override
    public BlockState updatePostPlacement(BlockState stateIn, Direction facing, BlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos) {
        if (stateIn.get(WATERLOGGED)) {
            worldIn.getPendingFluidTicks().scheduleTick(currentPos, Fluids.WATER, Fluids.WATER.getTickRate(worldIn));
        }

        return super.updatePostPlacement(stateIn, facing, facingState, worldIn, currentPos, facingPos);
    }

    @Override
    public FluidState getFluidState(BlockState state) {
        return state.get(WATERLOGGED) ? Fluids.WATER.getStillFluidState(false) : super.getFluidState(state);
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        switch (state.get(CANDLES)) {
            case 1:
            default:
                return ONE_CANDLE_SHAPE;
            case 2:
                return TWO_CANDLES_SHAPE;
            case 3:
                return THREE_CANDLES_SHAPE;
            case 4:
                return FOUR_CANDLES_SHAPE;
        }
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(CANDLES, LIT, WATERLOGGED);
    }

    @Override
    public boolean receiveFluid(IWorld worldIn, BlockPos pos, BlockState state, FluidState fluidStateIn) {
        if (!(Boolean) state.get(WATERLOGGED) && fluidStateIn.getFluid() == Fluids.WATER) {
            BlockState blockState = state.with(WATERLOGGED, true);
            if (state.get(LIT)) {
                extinguish(null, blockState, worldIn, pos);
            } else {
                worldIn.setBlockState(pos, blockState, 3);
            }

            worldIn.getPendingFluidTicks().scheduleTick(pos, fluidStateIn.getFluid(), fluidStateIn.getFluid().getTickRate(worldIn));
            return true;
        } else {
            return false;
        }
    }

    public static boolean canBeLit(BlockState state) {
        return state.isInAndMatches(CCBlockTags.CANDLES, (statex) -> {
            return statex.hasProperty(LIT) && statex.hasProperty(WATERLOGGED);
        }) && !state.get(LIT) && !state.get(WATERLOGGED);
    }

    @Override
    protected Iterable getParticleOffsets(BlockState state) {
        return (Iterable) CANDLES_TO_PARTICLE_OFFSETS.get(state.get(CANDLES));
    }

    @Override
    public boolean isValidPosition(BlockState state, IWorldReader worldIn, BlockPos pos) {
        return Block.hasEnoughSolidSide(worldIn, pos.down(), Direction.UP);
    }

}
