package net.threetag.craftablecomforts.block;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;

public interface ISitableBlock {

    float getSitOffset(BlockState state, BlockPos pos);

}
