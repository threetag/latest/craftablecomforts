package net.threetag.craftablecomforts.tileentity;

import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.threetag.craftablecomforts.CraftableComforts;
import net.threetag.craftablecomforts.block.CCBlocks;

public class CCTileEntityTypes {

    public static final DeferredRegister<TileEntityType<?>> TILE_ENTITIES = DeferredRegister.create(ForgeRegistries.TILE_ENTITIES, CraftableComforts.MODID);

    public static final RegistryObject<TileEntityType<?>> CRATE = TILE_ENTITIES.register("crate", () -> TileEntityType.Builder.create(CrateTileEntity::new, CCBlocks.OAK_CRATE.get(), CCBlocks.SPRUCE_CRATE.get(), CCBlocks.BIRCH_CRATE.get(), CCBlocks.JUNGLE_CRATE.get(), CCBlocks.ACACIA_CRATE.get(), CCBlocks.DARK_OAK_CRATE.get(), CCBlocks.CRIMSON_CRATE.get(), CCBlocks.WARPED_CRATE.get()).build(null));

}
