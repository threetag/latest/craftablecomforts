package net.threetag.craftablecomforts.tileentity;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.inventory.container.ChestContainer;
import net.minecraft.inventory.container.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ChestTileEntity;
import net.minecraft.tileentity.LockableLootTileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.NonNullList;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.threetag.craftablecomforts.block.CrateBlock;

import java.util.Iterator;

public class CrateTileEntity extends LockableLootTileEntity {

    private NonNullList<ItemStack> items;
    public int numPlayersUsing;

    public CrateTileEntity(TileEntityType<?> type) {
        super(type);
        this.items = NonNullList.withSize(27, ItemStack.EMPTY);
    }

    public CrateTileEntity() {
        this(CCTileEntityTypes.CRATE.get());
    }

    @Override
    public CompoundNBT write(CompoundNBT nbt) {
        super.write(nbt);
        if (!this.checkLootAndWrite(nbt)) {
            ItemStackHelper.saveAllItems(nbt, this.items);
        }

        return nbt;
    }

    @Override
    public void read(BlockState state, CompoundNBT nbt) {
        super.read(state, nbt);
        this.items = NonNullList.withSize(this.getSizeInventory(), ItemStack.EMPTY);
        if (!this.checkLootAndRead(nbt)) {
            ItemStackHelper.loadAllItems(nbt, this.items);
        }
    }

    @Override
    public int getSizeInventory() {
        return 27;
    }

    @Override
    public boolean isEmpty() {
        Iterator iterator = this.items.iterator();

        ItemStack stack;
        do {
            if (!iterator.hasNext()) {
                return true;
            }

            stack = (ItemStack) iterator.next();
        } while (stack.isEmpty());

        return false;
    }

    @Override
    public ItemStack getStackInSlot(int index) {
        return this.items.get(index);
    }

    @Override
    public ItemStack decrStackSize(int index, int amount) {
        return ItemStackHelper.getAndSplit(this.items, index, amount);
    }

    @Override
    public ItemStack removeStackFromSlot(int index) {
        return ItemStackHelper.getAndRemove(this.items, index);
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack) {
        this.items.set(index, stack);
        if (stack.getCount() > this.getInventoryStackLimit()) {
            stack.setCount(this.getInventoryStackLimit());
        }

    }

    @Override
    public void clear() {
        this.items.clear();
    }

    @Override
    protected NonNullList<ItemStack> getItems() {
        return this.items;
    }

    @Override
    protected void setItems(NonNullList<ItemStack> list) {
        this.items = list;
    }

    @Override
    protected ITextComponent getDefaultName() {
        return new TranslationTextComponent("container.craftablecomforts.crate");
    }

    @Override
    protected Container createMenu(int id, PlayerInventory playerInventory) {
        return ChestContainer.createGeneric9X3(id, playerInventory, this);
    }

    @Override
    public void openInventory(PlayerEntity player) {
        if (!player.isSpectator()) {
            if (this.numPlayersUsing < 0) {
                this.numPlayersUsing = 0;
            }

            ++this.numPlayersUsing;
            this.playSound(SoundEvents.BLOCK_BARREL_OPEN);
            this.scheduleTick();
        }

    }

    public void scheduleTick() {
        this.world.getPendingBlockTicks().scheduleTick(this.getPos(), this.getBlockState().getBlock(), 5);
    }

    public void crateTick() {
        int x = this.pos.getX();
        int y = this.pos.getY();
        int z = this.pos.getZ();
        this.numPlayersUsing = ChestTileEntity.calculatePlayersUsing(this.world, this, x, y, z);
        if (this.numPlayersUsing > 0) {
            this.scheduleTick();
        } else {
            BlockState state = this.getBlockState();
            if (!(state.getBlock() instanceof CrateBlock)) {
                this.remove();
                return;
            }

            this.playSound(SoundEvents.BLOCK_BARREL_CLOSE);
        }

    }

    @Override
    public void closeInventory(PlayerEntity player) {
        if (!player.isSpectator()) {
            --this.numPlayersUsing;
        }

    }

    public void playSound(SoundEvent sound) {
        double x = (double) this.pos.getX() + 0.5D;
        double y = (double) this.pos.getY() + 0.5D;
        double z = (double) this.pos.getZ() + 0.5D;
        this.world.playSound(null, x, y, z, sound, SoundCategory.BLOCKS, 0.5F, this.world.rand.nextFloat() * 0.1F + 0.9F);
    }
}