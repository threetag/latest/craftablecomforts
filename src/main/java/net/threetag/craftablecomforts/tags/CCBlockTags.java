package net.threetag.craftablecomforts.tags;

import net.minecraft.block.Block;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.Tags;

public class CCBlockTags {

    public static final Tags.IOptionalNamedTag<Block> CANDLES = BlockTags.createOptional(new ResourceLocation("minecraft", "candles"));
    public static final Tags.IOptionalNamedTag<Block> CANDLE_CAKES = BlockTags.createOptional(new ResourceLocation("minecraft", "candle_cakes"));

}
