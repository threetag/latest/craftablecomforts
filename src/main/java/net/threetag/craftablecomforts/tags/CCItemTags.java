package net.threetag.craftablecomforts.tags;

import net.minecraft.item.Item;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.Tags;

public class CCItemTags {

    public static final Tags.IOptionalNamedTag<Item> CANDLES = ItemTags.createOptional(new ResourceLocation("minecraft", "candles"));

}
